#!/usr/bin/python3

import re, sys, subprocess, glob

sys.stdout = open(sys.stdout.fileno(), mode='w', encoding='utf8', buffering=1)
# sys.stdout.reconfigure(encoding='utf-8') # Works only with Python 3.7+

# TODO remove
substitutes = {
    '≠': '\\neq',
    'λ': '\\lambda',
    '⊓': '\\sqcap',
    '⟦': '\\llbracket',
    '〚': '\\llbracket',
    '⟧': '\\rrbracket',
    '〛': '\\rrbracket',
    '⊗': '\\otimes',
    '⋅': '\\cdot',
    '·': '\\cdot',
    '»': '\\text\\guillemotright',
    '∧': '\\land',
}

# def substitute_listing(text):
#     return "".join("\001\\ensuremath{{{}}}\002".format(c) if ord(c)>127 else c
#                    for c in text)

def substitute_listing(text):
    return "".join("\001{}\002".format(c) if ord(c)>127 else c
                   for c in text)

def printerr(*args):
    print(args,file=sys.stderr)

def strip_comment(line):
    l = line.lstrip()
    if not l.startswith('#'):
        return None
    l = l[1:] # Strip the '#'
#    if l=='' or not l[0].isspace():
#        print(f'Malformed comment line "{line}"',file=sys.stderr)
#    return l.lstrip()
    return l

# def strip_heading(line):
#     l = line.lstrip()
#     if l.startswith('### '):
#         return l[4:]
#     return None

def is_blank(line): return line=='' or line.isspace()

def emit_code(code):
    if not code: return

    if code[-1]=='': code.pop()
    
    print()
    print("\\begin{lstlisting}")
    for l in code:
        if isinstance(l,int):
            print(f"\001\linenumber{{{l}}}\002", end='')
        else:
            l, _ = linkify(l)
            print(substitute_listing(l))
    print("\\end{lstlisting}")
    print()

def format_comment(comment):
    latex = subprocess.run(["pandoc",'-t','latex'], input=comment,
                           encoding='utf8',check=True,stdout=subprocess.PIPE).stdout
    return f"\\comment{{{latex}}}"

# def emit_heading(heading):
#     print()
#     print(f"\\subsection{{{format_comment(heading)}}}")
#     print()

def emit_comment(comment):
    if not comment: return
    comment = [f"\\linenumber{{{c}}}" if isinstance(c,int) else c for c in comment]
    print(format_comment("\n".join(comment)))

def emit_emacs_buffer_variables():
    print("""%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../listings"
%%% End:
""")

    
def readfile(filename):
    current_comment = []
    current_code = []
    linenumber = 0
    with open(filename,"rt") as f:
        for line in f.readlines():
            line = line.rstrip()
            linenumber += 1
            
            # heading = strip_heading(line)
            # if heading is not None:
            #     emit_code(current_code)
            #     current_code = []
            #     emit_comment(current_comment)
            #     current_comment = []
            #     emit_heading(heading)
            #     continue

            if re.match(r'^\s*#\s*GRAPH:',line):
                line = ''
            
            comment = strip_comment(line)
            if comment is not None:
                emit_code(current_code)
                current_code = []
                current_comment.append(linenumber)
                current_comment.append(comment)
                continue
            
            if is_blank(line):
                emit_comment(current_comment)
                current_comment = []

                if current_code and current_code[-1] == '':
                    current_code.pop()

                current_code.append(linenumber)
                current_code.append('')
                
                continue

            emit_comment(current_comment)
            current_comment = []
            current_code.append(linenumber)
            current_code.append(line)
            
        emit_comment(current_comment)
        emit_code(current_code)

        emit_emacs_buffer_variables()

def linkify(line):
    
    def links(str):
        if line.startswith("include"): return str
        guess_prog = re.search(r"\b(?:call|inline)\b",str) is not None

        def link_name(m):
            global known_lemmas, known_programs
            name = m.group(0)

            if name in known_lemmas:
                if guess_prog and name in known_programs:
                    type = 'program'
                else:
                    type = 'lemma'
            elif name in known_programs:
                type = 'program'
            else:
                #printerr(f"link_name: nope {name}")
                #printerr(known_programs)
                return name

            #print(f"link_name: {type} {name}", file=sys.stderr)

            # TODO: make links
            return name
            
        return re.sub(r"[a-zA-Z0-9_']+",link_name,str)
    
    def anchor(type, m):
        start,name,end = m.groups()
        line2 = f"{start}\label{{{type}:{name}}}{name}{links(end)}"
        return line2, f"{type}#{name}"

    m = re.match(r"(^\s*lemma\s+)([a-zA-Z0-9'_]+)(\s*:.*$)", line, re.DOTALL)
    if m: return anchor('lemma', m)
    m = re.match(r"(^\s*qrhl\s+)([a-zA-Z0-9'_]+)(\s*:.*$)", line, re.DOTALL)
    if m: return anchor('lemma', m)
    m = re.match(r"(^\s*program\s+)([a-zA-Z0-9'_]+)(\s*[:(].*$)", line, re.DOTALL)
    if m: return anchor('program', m)
    m = re.match(r"(^\s*adversary\s+)([a-zA-Z0-9'_]+)(\s.*$)", line, re.DOTALL)
    if m: return anchor('program', m)

    return links(line), None
    
def scan():
    seen = set()
    
    for fn in glob.glob("proofs/*.qrhl"):
        with open(fn,"rt") as f:
            for l in f.readlines():
                l2,name = linkify(l)
                if not name: continue
                if name in seen:
                    sys.exit(f"Duplication: {name} in {fn}")
                seen.add(name)
                print(name)

known_lemmas = set()
known_programs = set()
def readknownnames():
    global known_lemmas, known_programs
    with open("tex/known-names.txt","rt") as f:
        for l in f.readlines():
            type,name = l.strip().split('#')
            if type == 'lemma':
                known_lemmas.add(name)
            elif type == 'program':
                known_programs.add(name)
            else:
                raise f"Bad type: {type}"
                
if sys.argv[1] == 'SCAN':
    scan()
else:
    readknownnames()
    readfile(sys.argv[1])
