
set -o pipefail

if [ "$1" != "" ]; then
    if ! (cd ../qrhl-tool && ./build.sh); then
	notify-send "QRHL build failed"
	play -q -n -t alsa synth 0.1 tri 1000
	echo "(qrhl-tool build)" >> errors.log
	exit 1
    fi

    if ! (cd ../qrhl-tool && ./test.sh); then
	notify-send "QRHL tests failed"
	play -q -n -t alsa synth 0.1 tri 1000
	echo "(qrhl-tool tests)" >> errors.log
	exit 1
    fi
fi

if ! make test |& tee test.log; then
    notify-send "FO tests failed"
    play -q -n -t alsa synth 0.1 tri 1000
    ERROR_LINE=`grep '^\[ERROR\] .*:[0-9]\+:' test.log | tail -1`
    TMP="${ERROR_LINE#\[ERROR\] }"
    LOC="${TMP%:*}"
    echo "Error in $LOC"
    echo "$LOC" >> errors.log
    exit 1
fi

notify-send "Tests done"
play -q -n -t alsa synth 0.1 tri 440
echo "(success)" >> errors.log
