#!/bin/bash

set -e

DIR="$(dirname "$BASH_SOURCE[0]")"

if [ "$#" = 0 ]; then
    FILES=("$DIR/proofs/All.thy")
else
    FILES=()
fi

SESSION=Lots-Of-Stuff
#SESSION=QRHL-Prerequisites
#SESSION=QRHL

ISABELLE_DIR=/opt/Isabelle2021-1
QRHL_DIR=../qrhl-tool

"$ISABELLE_DIR/bin/isabelle" jedit -l "$SESSION" -d "$QRHL_DIR" "${FILES[@]}" "$@" &
