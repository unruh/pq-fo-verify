
.DELETE_ON_ERROR :

doc : listings.pdf

test0:
	python3 replace.py
	make doc
	ls -1 proofs/*.qrhl | shuf | sed -e 's/.*/include "&"./' >tocheck.qrhl
	time ~/r/qrhl-tool/bin/qrhl tocheck.qrhl

test: test0
	-git update-index --refresh # Otherwise git describe may add --index in a clean wc with changed mtimes
	-git  -C ~/r/qrhl-tool update-index --refresh
	( echo -n "Proof:          " && git describe --tags --long --always --dirty --broken && echo -n "QRHL Tool:      " && git -C ~/r/qrhl-tool describe --tags --long --always --dirty --broken && echo -n "Date:           " && date ) > last-success-dirty.txt
	if ! grep -- '-dirty$$' last-success-dirty.txt; then mv -v last-success-dirty.txt last-success.txt; fi

QRHL_FILES=$(wildcard proofs/*.qrhl)
#QRHL_FILES=proofs/lemma_decapsQuery1_1G.qrhl
QRHL_FILES_BASENAMES=$(QRHL_FILES:proofs/%.qrhl=%)
QRHL_TEX_FILES=$(QRHL_FILES:proofs/%.qrhl=tex/%.qrhl.tex)

tex/files.tex : $(QRHL_FILES)
	echo -e $(QRHL_FILES_BASENAMES:%="\load{%}\n") >$@

listings.pdf : listings.tex $(QRHL_TEX_FILES) Makefile tex/files.tex
	pdflatex -interaction batchmode listings.tex

tex/known-names.txt : $(QRHL_FILES) qrhl2tex.py Makefile
	mkdir -p $(dir $@)
	python3 qrhl2tex.py SCAN > $@

tex/%.qrhl.tex : proofs/%.qrhl qrhl2tex.py Makefile
	mkdir -p $(dir $@)
	#make tex/known-names.txt
	> tex/known-names.txt
	python3 qrhl2tex.py "$<" >"$@"

cloc :
	cloc --read-lang-def cloc-lang-def --out=/dev/stdout proofs/
	./remove-autogen
	cloc --read-lang-def cloc-lang-def --out=/dev/stdout proofs/

pqfo-verify.zip :
	rm -rf pqfo-verify
	rm ../qrhl-tool/doc/manual.pdf
	git clean -fdX
	git clean -id
	mkdir pqfo-verify
	python3 replace.py
	cp proofs/* pqfo-verify
	make -C ../qrhl-tool qrhl.zip
	cd pqfo-verify && unzip ../../qrhl-tool/qrhl.zip
	cd pqfo-verify && mv qrhl-0.5alpha qrhl-tool
	cp README-archive pqfo-verify/README.txt
	rm -f $@
	#if grep sorry pqfo-verify/*.thy; then false; else true; fi
	if grep --exclude=axioms.qrhl admit pqfo-verify/*.qrhl; then false; else true; fi
	zip -r $@ pqfo-verify

update-hksu-repo:
	rm -f ../hksu-verification/*.qrhl ../hksu-verification/*.thy
	cp proofs/*.qrhl proofs/*.thy ../hksu-verification/
